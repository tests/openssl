#!/bin/sh

usage () {
	echo "usage: $0 [add|pull]"
	echo ""
	echo "'$0 add' adds the common subtree to a tests repository"
	echo "'$0 pull' is used to update the common subtree in tests"
}

branch="$(git symbolic-ref --short HEAD)"
case "$branch" in
	apertis/*)
		;;
	*)
        # take the default remote branch if we're not on a apertis/* branch
		branch=$(git ls-remote --symref git@gitlab.apertis.org:tests/common.git HEAD \
		         | grep ^ref: \
		         | sed 's,ref: refs/heads/\(.*\)\s\+HEAD,\1,')
		;;
esac

case $1 in
pull)
git subtree pull -P common git@gitlab.apertis.org:tests/common.git "$branch"
;;
add)
git subtree add -P common git@gitlab.apertis.org:tests/common.git "$branch"
;;
*) usage;;
esac
